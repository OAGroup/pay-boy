package com.ndood.authenticate.app.social;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.social.security.SocialAuthenticationFilter;
import org.springframework.stereotype.Component;

import com.ndood.core.social.SocialAuthenticationFilterPostProcessor;

/**
 * 授权码模式-在app模块对core中的PostProcessor接口实现
 * 在浏览器环境下为空。在app环境下实现.app登录成功后返回json
 */
@Component
public class AppSocialAuthenticationFilterPostProcessor implements SocialAuthenticationFilterPostProcessor {

	@Autowired
	private AuthenticationSuccessHandler appAuthenticationSuccessHandler;

	@Override
	public void process(SocialAuthenticationFilter socialAuthenticationFilter) {
		socialAuthenticationFilter.setAuthenticationSuccessHandler(appAuthenticationSuccessHandler);
	}
	
}