package com.ndood.authenticate.browser.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ndood.core.properties.CommonConstants;
import com.ndood.core.support.SimpleResponse;

/**
 * 退出登录成功处理器
 */
public class BrowserLogoutSuccessHandler implements LogoutSuccessHandler{
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	private String signOutUrl;
	
	private ObjectMapper objectMapper = new ObjectMapper();
	
	public BrowserLogoutSuccessHandler(String signOutUrl) {
		this.signOutUrl = signOutUrl;
	}

	/**
	 * 退出登录成功处理
	 */
	@Override
	public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
		logger.info("退出成功!");
		if(StringUtils.isBlank(signOutUrl)){
			response.setContentType("application/json;charset=UTF-8");
			response.getWriter().write(objectMapper.writeValueAsString(new SimpleResponse(CommonConstants.SUCCESS, "退出成功")));
			response.getWriter().flush();
		} else {
			response.sendRedirect(signOutUrl);
		}
	}
	
}