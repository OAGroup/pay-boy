package com.ndood.core.properties;

/**
 * Payment常量
 * @author ndood
 */
public interface CommonConstants {
	/**
	 * 返回码
	 */
	String SUCCESS = "10000";
	String FAILUE = "-1";
	
	/**
	 * 验证码前缀
	 */
	String SESSION_KEY_PREFIX = "SESSION_KEY_";
	
}
