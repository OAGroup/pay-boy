define([
	'jquery',
	'jquery-validation',
	'jquery-extension',
	'layui',
	'bootstrap-table',
	'bootstrap-table-zh-CN',
	'bootstrap-table-mobile',
], function() {
	return {
		initUpdateDialog: function(){
		},
		initAddDialog: function(){
		},
		initPage: function(){
			// 1 初始化表单控件
			$('#job_datatable').bootstrapTable({
				url : "/system/job/query", 
				method : 'post', 
				toolbar : '#job_toolbar',
				uniqueId: "id",
				striped: true,
				cache: false,
				pagination : true, 
				sortable: false,
				singleSelect : false,
				search: true,
				showColumns: true,
				showToggle:false,
				cardView: false,
				showRefresh: true,
				clickToSelect: true,
				minimumCountColumns: 5,
				queryParams : function(params) {
					return {
						limit : params.limit,
						offset : params.offset,
						keywords: params.search,
						sortName:'sort'
					};
				},
				sidePagination : "server",
				pageNumber:1, 
				pageSize : 10,
				pageList: [10, 20, 30],
				columns : [
					{
						checkbox : true
					},
					{
						field : 'id',
						title : '编号'
					},
					{
						field : 'jobName',
						title : '任务名'
					},
					{
						field : 'beanClass',
						title : '任务bean'
					},
					{
						visible : false,
						field : 'springBean',
						title : 'springBean'
					},
					{
						field : 'jobGroup',
						title : '任务组'
					},
					
					{
						field : 'description',
						title : '描述'
					},
					{
						visible : false,
						field : 'createBy',
						title : '创建人'
					},
					{
						visible : false,
						field : 'updateBy',
						title : '修改人'
					},
					{
						field : 'cronExpression',
						title : '表达式'
					},
					{
						field : 'methodName',
						title : '方法名',
					},
					{
						visible: false,
						field : 'isConcurrent',
						title : '是否同步',
					},
					{
						field : 'jobStatus',
						title : '状态',
						formatter: function(value, row, index){
							if(value==0){
								return '<span class="text-danger"><i class="fa fa-circle"></i> 未启动</span>';
							}else{
								return '<span class="text-success"><i class="fa fa-circle"></i> 启动</span>';
							}
						}
					},
					{
						visible: false,
						field : 'createTime',
						width : '150px',
						title : '创建时间'
					},
					{
						field : 'updateTime',
						width : '150px',
						title : '最后修改时间'
					},
					{
						title : '操作',
						field : '',
						align : 'center',
						formatter : function(value, row, index) {
							var b1 = '<a href="javascript:void(0);" class="btn btn-xs btn-success btn-editone" onclick="job_open_update_dialog('+row['id']+')"><i class="fa fa-pencil"></i></a>';
							var b2 = '<a href="javascript:;" class="btn btn-xs btn-success btn-delone" onclick="job_delete('+row['id']+')"><i class="fa fa-trash"></i></a>';
							var b3 = '<a href="javascript:;" class="btn btn-xs btn-success btn-delone" onclick="job_stop('+row['id']+')">停止</a>';
							if(row.jobStatus=='0'){
								var b3 = '<a href="javascript:;" class="btn btn-xs btn-success btn-delone" onclick="job_start('+row['id']+')">启动</a>';
							}
							return b1 + b2 + b3;
						}
					} 
				]
			});
			
			// 初始化手机响应式
			var docW = window.innerWidth;
			if (docW < 768) {
				$("#job_search_plus").show();
				$("#job_search_plus_btn").hide();
				$("#job_content .bootstrap-table .fixed-table-toolbar input[type='text']").hide();
			}else{
				$("#job_search_plus").hide();
			}
		}
	}
});
// 添加角色
function job_open_add_dialog() {
	var docW = window.innerWidth;
	var area = [ '100%', '100%' ];
	var maxmin = false;
	if (docW >= 768) {
		area = [ '800px', '600px' ];
		maxmin = true;
	}
	var i = layer.open({
		type : 2,
		title : '添加角色',
		maxmin : maxmin,
		shadeClose : false,
		area : area,
		content : '/system/job/add',
		btn : [ '保存+btn-success+fa-check', '取消+btn-success+fa-close' ],
		btn1 : function(index) {
			var loadindex = layer.load(1, {time: 5*1000});	
			$("#layui-layer-iframe" + i)[0].contentWindow.add_job();
			layer.close(loadindex);
		},
		btn2 : function(index) {
			layer.close(i);
		}
	});
	if (docW >= 768) {
		layer.restore(i);
	}else{
		layer.full(i);
	}
}
//删除角色
function job_delete(id){
	layer.confirm('确定要删除吗？', {
		btn : [ '确定', '取消' ]
	}, function() {
		$.ajax({
			url : '/system/job/delete',
			type : "post",
			data : {'id' : id},
			success : function(data) {
				if (data.code != '10000') {
					layer.alert(data.msg,{icon: 2});
					return;
				}
				layer.alert(data.msg, {icon: 1});
				job_reload();
			},
	        error: function (response, ajaxOptions, thrownError) {
	        	errorCallBack(response, ajaxOptions, thrownError);                
	        }
		});
	})
}
// 批量删除
function job_batch_delete(){
	var rows = $('#job_datatable').bootstrapTable('getSelections'); 
	if (rows.length == 0) {
		layer.alert("请至少选择一条记录!",{icon: 7});
		return;
	}
	layer.confirm("确定要删除选中的'" + rows.length + "'条记录吗?", {
		btn : [ '确定', '取消' ]
	}, function() {
		var ids = [];
		$.each(rows, function(i, row) {
			ids.push(row['id']);
		});
		$.ajax({
			type : 'post',
			data : {ids:ids},
			dataType : 'json',
			url : '/system/job/batch_delete',
			success : function(data) {
				if (data.code != '10000') {
					layer.alert(data.msg,{icon: 2});
					return;
				}
				layer.alert(data.msg, {icon: 1});
				job_reload();
			},
	        error: function (response, ajaxOptions, thrownError) {
	        	errorCallBack(response, ajaxOptions, thrownError);                
	        }
		});
	}, function() {});
}
// 修改角色
function job_open_update_dialog(id){
	var docW = window.innerWidth;
	var area = [ '100%', '100%' ];
	var maxmin = false;
	if (docW >= 768) {
		area = [ '800px', '600px' ];
		maxmin = true;
	}
	var i = layer.open({
		type : 2,
		title : '修改角色',
		maxmin : maxmin,
		shadeClose : false,
		area : area,
		content : '/system/job/update?id='+id,
		btn : [ '保存+btn-success+fa-check', '取消+btn-success+fa-close' ],
		btn1 : function(index) {
			var loadindex = layer.load(1, {time: 5*1000});	
			$("#layui-layer-iframe" + i)[0].contentWindow.update_job();
			layer.close(loadindex);
		},
		btn2 : function(index) {
			layer.close(i);
		}
	});
	if (docW >= 768) {
		layer.restore(i);
	}else{
		layer.full(i);
	}
}
// 重新加载datatable
function job_reload(){
	$('#job_datatable').bootstrapTable('refresh', null);
}
//保存地区
function add_job(callback){
	// Step1: 表单验证
	var valid = $("#job_add_form").valid();
	if(!valid){
		return;
	}
	
	// Step2: 获取输入数据，拼装成请求数据
	var data = $('#job_add_form').serializeJson();
	var arr =  $("#job_tree").jstree(true).get_selected();
	data['resourceIds'] = arr;
	
	// Step3: 提交表单
    $.ajax({
    	url:"/system/job/add",
        type:"post",
        dataType:'json',
        contentType:"application/json;charset=utf-8",
        data:JSON.stringify(data),
        async:false,
        success:function(data){
        	if(data.code!='10000'){
        		parent.layer.alert(data.msg, {icon: 2});
        		var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
    			parent.layer.close(index);
        		return;
        	}
        	parent.layer.alert(data.msg, {icon: 1});
        	parent.job_reload();
        	var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
			parent.layer.close(index);
        },
        error: function (response, ajaxOptions, thrownError) {
        	errorCallBack(response, ajaxOptions, thrownError);                
        }
    });
}
//保存角色
function update_job(callback){
	// Step1: 表单验证
	var valid = $("#job_update_form").valid();
	if(!valid){
		return;
	}
	
	// Step2: 获取输入数据，拼装成请求数据
	var data = $('#job_update_form').serializeJson();
	var arr =  $("#job_tree").jstree(true).get_selected();
	data['resourceIds'] = arr;
	
	// Step3: 提交表单
    $.ajax({
    	url:"/system/job/update",
        type:"post",
        dataType:'json',
        contentType:"application/json;charset=utf-8",
        data:JSON.stringify(data),
        async:false,
        success:function(data){
        	if(data.code!='10000'){
        		parent.layer.alert(data.msg, {icon: 2});
        		var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
    			parent.layer.close(index);
        		return;
        	}
        	parent.layer.alert(data.msg, {icon: 1});
        	parent.job_reload();
        	var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
			parent.layer.close(index);
        },
        error: function (response, ajaxOptions, thrownError) {
        	errorCallBack(response, ajaxOptions, thrownError);                
        }
    });
}
// 启动定时任务
function job_start(id){
	layer.confirm('确定启动任务吗？', {
		btn : [ '确定', '取消' ]
	}, function() {
		$.ajax({
			url : '/system/job/change_status',
			type : "post",
			data : {'id' : id, 'status': '1'},
			success : function(data) {
				if (data.code != '10000') {
					layer.alert(data.msg,{icon: 2});
					return;
				}
				layer.alert('启动任务成功！', {icon: 1});
				job_reload();
			},
	        error: function (response, ajaxOptions, thrownError) {
	        	errorCallBack(response, ajaxOptions, thrownError);                
	        }
		});
	})
}
// 停止定时任务
function job_stop(id){
	layer.confirm('确定要停止任务吗？', {
		btn : [ '确定', '取消' ]
	}, function() {
		$.ajax({
			url : '/system/job/change_status',
			type : "post",
			data : {'id' : id, 'status': '0'},
			success : function(data) {
				if (data.code != '10000') {
					layer.alert('停止任务成功！',{icon: 2});
					return;
				}
				layer.alert(data.msg, {icon: 1});
				job_reload();
			},
	        error: function (response, ajaxOptions, thrownError) {
	        	errorCallBack(response, ajaxOptions, thrownError);                
	        }
		});
	})
}