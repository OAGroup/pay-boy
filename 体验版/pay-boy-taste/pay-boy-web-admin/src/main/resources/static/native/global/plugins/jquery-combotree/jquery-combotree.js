(function($) {
	"use strict";
	// 用于缓存options配置
	$.fn.ndoodComboTree = function(options, param, param2) {
		// 如果options为方法，则调用字符串对应的方法
		if (typeof options == 'string') {
			return $.fn.ndoodComboTree.methods[options](this, param, param2);
		}

		// 如果options为json，则根据json进行初始化
		options = $.extend({}, $.fn.ndoodComboTree.defaults, options || {});
		
		var target = $(this);
		target.prop('temp_options',options);
		
		$.fn.ndoodComboTree.initComboTree(target,options);
		return target;
	};
	
	// 初始化combotree
	$.fn.ndoodComboTree.initComboTree = function(target, options){
		// 获取初始值
		if(!target.hasClass('combotree')){
			target.addClass('combotree');
		}
		var init_value = target.attr("init_value");
		var id = init_value == undefined || init_value == '' ? '' : init_value;
		// target.removeAttr("init_value"); 方便reset后重置
		
		target.html('<input type="text" style="display:none" id="combotree_hidden" name="'+options.key+'" value="'+id+'">'+
			'<input id="combotree_input" class="form-control input-sm" type="text" readOnly="true" autocomplete="off" placeholder="'+options.placeholder+'">'+
			'<div class="input-group-btn">'+
			'	<button type="button" id="combotree_btn" class="btn btn-white btn-sm">'+
			'		<span class="caret"></span>'+
			'	</button>'+
			'</div>'+
			'<div id="combotree_treeview" style="position: absolute; display:none; left: 0;width:100%;z-index: 10000;background: white;border:1px solid lightgray;margin-top:30px;top:0px;"></div>');
		
		// ajax加载数据并生成树结构
		$.ajax({
			type : 'post',
			url : options.url,
			data : {},
			dataType : "JSON",
			async: true,
			success : function(res, textStatus, jqXHR) {
				if(res.code!='10000'){
					return;
				}
				target.find("#combotree_treeview").on('redraw.jstree', function (e) {
					$(".layer-footer").attr("domrefresh", Math.random());
				}).jstree({
					"themes": {"stripes": false},
					"core": {
						'check_callback': true,
						"data": res.data
					}
				}).bind("activate_node.jstree",function(obj, e){
					var id = e.node.id;
					var text = e.node.text;
					target.find("#combotree_hidden").val(id);
					target.find("#combotree_input").val(text);
				});
				// 初始化选中事件 TODO jstree根据id获取node不成功
				if(id!=''){
					for(var index in res.data){
						if(res.data[index].id==id){
							var text = res.data[index].text;
							target.find("#combotree_input").val(text);
						}
					}
				}
				// 点击input框显示
				target.find("#combotree_input,#combotree_btn").on('click',function(){
					var treeview = target.find("#combotree_treeview");
					if(treeview.is(":hidden")){
						treeview.show();
					}
				});
				// 点击空白处隐藏
				$(document).click(function(event){
					var treeview = target.find("#combotree_treeview");
					if(!target.is(event.target) && target.has(event.target).length === 0){
						treeview.hide();
					}
				});
				return false;
			}
		});
	}
	
	// 自定义方法列表
	$.fn.ndoodComboTree.methods = {
		reload: function(target){
			var options = target.prop('temp_options');
			$.fn.ndoodComboTree.initComboTree(target,options);
		}
	};
})(jQuery);