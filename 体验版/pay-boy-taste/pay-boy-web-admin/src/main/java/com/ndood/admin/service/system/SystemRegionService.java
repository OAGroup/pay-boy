package com.ndood.admin.service.system;

import java.util.List;

import com.ndood.admin.pojo.system.dto.RegionDto;

/**
 * 区域管理业务接口
 * @author ndood
 */
public interface SystemRegionService {

	/**
	 * 获取区域tree列表
	 * @param keywords 
	 * @param parentId 
	 * @param query
	 * @return
	 * @throws Exception 
	 */
	List<RegionDto> getRegionList(String keywords, Integer parentId) throws Exception;

	/**
	 * 批量删除地区
	 * @param ids
	 */
	void batchDeleteRegion(Integer[] ids);

	/**
	 * 添加地区
	 * @param region
	 * @return 
	 * @throws Exception 
	 */
	RegionDto addRegion(RegionDto region) throws Exception;

	/**
	 * 获取地区信息
	 * @param id
	 * @return
	 * @throws Exception 
	 */
	RegionDto getRegion(Integer id) throws Exception;

	/**
	 * 更新地区信息
	 * @param region
	 * @return 
	 * @throws Exception 
	 */
	RegionDto updateRegion(RegionDto region) throws Exception;

	/**
	 * 获取省数据
	 * @param keywords
	 * @param parentId
	 * @return
	 * @throws Exception 
	 */
	List<RegionDto> getProvinceList(String provinceId) throws Exception;

}
