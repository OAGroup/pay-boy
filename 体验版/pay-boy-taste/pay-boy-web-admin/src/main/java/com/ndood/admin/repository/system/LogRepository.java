package com.ndood.admin.repository.system;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ndood.admin.pojo.system.LogPo;

/**
 * 日志dao
 */
public interface LogRepository extends JpaRepository<LogPo, Integer>{

	Page<LogPo> findAll(Specification<LogPo> specification, Pageable pageable);
	
}
