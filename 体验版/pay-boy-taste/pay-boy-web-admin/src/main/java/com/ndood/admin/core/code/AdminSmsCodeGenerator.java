package com.ndood.admin.core.code;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.ServletWebRequest;

import com.ndood.core.properties.SecurityProperties;
import com.ndood.core.validate.code.ValidateCode;
import com.ndood.core.validate.code.ValidateCodeGenerator;

/**
 * 新建验证码生成器，覆盖core ValidateCodeBeanConfig中默认的
 * @author ndood
 */
@Component("smsCodeGenerator")
public class AdminSmsCodeGenerator implements ValidateCodeGenerator{
	
	@Autowired
	private SecurityProperties securityProperties;

	public SecurityProperties getPaymentProperties() {
		return securityProperties;
	}

	public void setPaymentProperties(SecurityProperties securityProperties) {
		this.securityProperties = securityProperties;
	}

	/**
	 * 生成短信验证码
	 */
	@Override
	public ValidateCode generate(ServletWebRequest request) {
		String code = RandomStringUtils.randomNumeric(securityProperties.getCode().getSms().getLength());
		return new ValidateCode(code,securityProperties.getCode().getSms().getExpireIn());
	}
	
}