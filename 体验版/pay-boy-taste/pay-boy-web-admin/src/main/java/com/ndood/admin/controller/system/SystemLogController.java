package com.ndood.admin.controller.system;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ndood.admin.pojo.comm.dto.DataTableDto;
import com.ndood.admin.pojo.system.query.LogQuery;
import com.ndood.admin.service.system.SystemLogService;

/**
 * 日志控制器类
 * @author ndood
 */
@Controller
public class SystemLogController {
	
	@Autowired
	private SystemLogService systemLogService;
	
	/**
	 * 显示角色页
	 */
	@GetMapping("/system/log")
	public String toRolePage(){
		return "system/log/log_page";
	}
	
	/**
	 * 角色列表
	 * @throws Exception 
	 */
	@PostMapping("/system/log/query")
	@ResponseBody
	public DataTableDto getDectListPage(@RequestBody LogQuery query) throws Exception{
		DataTableDto page = systemLogService.pageLogList(query);
		return page;
	}
	
}
