package com.ndood.admin.pojo.system.query;

import java.util.Date;

import com.ndood.admin.pojo.system.UserPo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * 用户查询类
 * @author ndood
 */
@Getter @Setter
@EqualsAndHashCode(callSuper=false)
public class UserQuery extends UserPo {
	private static final long serialVersionUID = 7617102163083523409L;
	private Integer offset;
	private Integer limit;
	private String keywords;
	private Date startTime;
	private Date endTime;
	private Integer deptId;
	private Integer sex;

	public int getPageNo() {
		return offset / limit;
	}
}
